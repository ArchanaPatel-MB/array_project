const find=(Element,callback)=>{
    for(let index=0;index<Element.length;index++){
        if(callback(Element[index],find,Element)){
            return (Element[index]);
        }
    }
    return undefined;
}
module.exports=find;