const filter = (Element, cb) => {
    if (typeof Element == 'undefined') return "undefined";
    let arr_greaterThan_two = [];
    for (let i = 0; i < Element.length; i++) {
        if (cb(Element[i], i, Element)) {
            arr_greaterThan_two.push(Element[i]);
        }
    }
    return arr_greaterThan_two;
}
module.exports = filter;