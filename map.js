const map = (Element, callback) => {
    let new_array = [];
    for (let index = 0; index < Element.length; index++) {
        new_array.push(callback(Element[index], index,Element));
    }
    return new_array;
}
module.exports = map;