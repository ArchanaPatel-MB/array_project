const reduce = (element, callback, starting_value) => {
    if (typeof element == 'undefined') return;

    if (typeof starting_value == 'undefined'){
        for (let index = 0; index < element.length; index++) {
              starting_value = callback(starting_value, element[index], index, element);
                }
                return starting_value;
            }
    else {
    let starting_value = 0;
    for (let index = 0; index < element.length; index++) {
        starting_value = callback(starting_value, element[index], index, element);
    }
    return starting_value;
}
}
module.exports = reduce;